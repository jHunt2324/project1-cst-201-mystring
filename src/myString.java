public class myString {
    private char[] arr;
    private int curr_length;
    private int arr_size;

    //Constructor with no parameters
    myString() {
        this.arr = null;
        this.curr_length = 0;
    }

    // initial constructor with parameter
    public myString(String s) {
        this.curr_length = s.length();
        this.arr = s.toCharArray();
         arr_size = this.curr_length;

    }

    //copy constructor made this so we
    //can call the toUpper and Lower
    public myString(myString string) {
        this.curr_length = string.curr_length;
        this.arr = string.arr;
    }

    //Constructor so we can use concat method
    public myString(char[] inchar) {
        this.curr_length = inchar.length;
        this.arr = inchar;
    }

    //return the length of the array
    int length() {
      return curr_length;
  }

    //prints out the myString
    void goString() {
        for(int i =0; i < curr_length; i++) {
            System.out.print(arr[i]);
        }
  }

    //compares two strings together
    boolean equals(myString arr) {
        if(arr.curr_length == this.arr.length) { //checks to make sure strings are equal length
            for(int i = 0; i<curr_length; i++) { //Loop through the strings
                if(arr.arr[i] != this.arr[i]) { //If there is no match break out of the loop
                    break;
                }
            }
            return true; //return true if they match
        }
        return false; //return false if they do not match
    }

    //puts the string into uppercase
    myString toUpper(myString str) {
        myString string = new myString(str); //create a new mystring object

        for(int i = 0; i < string.curr_length; i++) { //Loop through the string
            if('a'<= string.arr[i] && string.arr[i] <= 'z') { //checks to make sure that the characters are in the array
                string.arr[i] = (char)(string.arr[i] -'a' +'A'); //Puts the characters into uppercase.
            }
        }
        return string; //return our string.
    }

    //puts the string into lowercase
    myString toLower(myString str) {
        myString string = new myString(str); //create a new mystring

        for(int i = 0; i < string.curr_length; i++) { //Loop through the array
            if('A'<= string.arr[i] && string.arr[i] <= 'Z') { //checks the words that our uppercase
                string.arr[i] = (char)(string.arr[i] +32); //moves them into lowercase
            }
        }
        return string; //return that string.
    }

    //Find which letter comes first in the alphabet
    int compareTo(myString s) {
        for(int i =0; i < s.curr_length; i++) { //Loops through the array.
            if(arr[i] > s.arr[i]) { //checks to see which letter alphbetically comes first
                return -1;
            }
            if(arr[i] < s.arr[i]) { //If it is smaller number that means the letters come first
                return 1; // 'a'=97,'b'=98
            }
        }
        return 0;
    }

    //returns a character where the user searches
    char get(int i) {
        if(i < arr.length) {
            return arr[i];
        }
        System.out.println("Index out of bounds");
        return ' ';
    }

    //returns string from position b
    //to the end of the string
    myString substring(int b){
        String s = ""; // create a variable to old our characters

        for(int i = b; i < arr.length; i++) { //loop through the array at the position we give
            s += arr[i]; //fill our string with those arrays
        }

        return new myString(s); //return the string to myString
    }

    //return a string from position b to position e
    //same as the one above but we stop at a certain index
    myString substring(int b, int e) {
        String s = " ";

        for(int i =b; i<=e; i++) {
            s += arr[i];
        }

        return new myString(s);
    }

    // returns the index of where the String is found in the class.
    // I have notes on this ill talk through them in loom.
    int indexOf(String str) {
        //Loop through our current word
        for(int i =0; i< curr_length; i++) {
            int j;
            for (j = 0; j < str.length(); j++) { //loop through our String
                if (arr[i + j] != str.charAt(j)) { //If the strings do not match break out of loop
                    break;
                }
            }
            if(j==str.length()) { //when j == str.length we return what our index of I because thats where the loop is.
                return i;
            }
        }

        return -1;
    }

    // This is wrong But I am going to turn it in anyways.
    int lastIndexOf(String str) {
        for(int i =0; i<curr_length; i++) {
            int j;

            for(j =0; j<str.length(); j++) {
                if(arr[i+j] != str.charAt(j)) {
                    break;
                }
            }
            if(j == str.length()) {
                return (i +str.length());
            }
        }
        return -1;
    }

    //Make a dynamic array
    private void ensureCapacity() {
        char[] temp = new char[arr_size* 2]; //create a temporary array

        for(int i=0; i<arr_size; i++) {
            temp[i] = arr[i]; //fill our temporary array with our first array
        }

        arr = new char[arr_size * 2]; //create our new array with a bigger size

        arr_size = arr_size * 2; //create the array size to be bigger

        for(int i =0; i<arr_size; i++) {
            arr[i] = temp[i]; //Fill our array back with our initial characters
        }
    }

    //Put the two arrays together
    public myString concat(myString str) {
        ensureCapacity(); //call ensure capacity to make array bigger

        int totalLength = this.arr.length + str.arr.length; //get the length that we need

        char[] result2 = new char[totalLength]; //call our new array
        for(int i=0;i<totalLength;i++){ //loop through the total number of characters
            if(i>=this.arr.length){ //if i> then the length
                result2[i]=str.arr[i-this.arr.length]; //we fill our array with the second word
            }else{
                result2[i]=this.arr[i]; // we fill our array with the first word
            }
        }
        return new myString(result2); // we return a mystring of the char array.
    }


}